package cn.itbat.whh.usercenter.enums;

/**
 * 本类是字典表的枚举类集合，由工具类生成
 * @author EnumUtil.java
 * @Datetime 2018-04-13 14:17:52
 */
public class UserCenterDefinedCode {
    public enum TEST {
        TEST("TEST", "TEST", "TEST"),
        ;
        private String code;
        private String name;
        private String handleCode;

        TEST(String code, String name, String handleCode) {
            this.code = code;
            this.name = name;
            this.handleCode = handleCode;
        }

        public String getCode() {
            return this.code;
        }

        public String getName() {
            return this.name;
        }

        public String getHandleCode() {
            return this.handleCode;
        }

        @Override
        public String toString() {
            return ("handleCode = " + this.handleCode + ",name = " + this.name + ",code = " + this.code);
        }

        public boolean equalByCode(String code) {
            return this.code != null && this.code.equals(code);
        }

        public static TEST getByCode(String code) {
            for (TEST item : values()) {
                if (item.code.equals(code)) {
                    return item;
                }
            }
            return null;
        }
    }
}
