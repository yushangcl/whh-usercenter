package cn.itbat.whh.usercenter.service;

import cn.itbat.whh.usercenter.model.CmUser;

/**
 * @author huahui.wu.
 *         Created on 2018/3/16.
 */
public interface CmUserService {
    /**
     * 根据用户ID查询用户信息
     * @param userId 用户ID
     * @return CmUser
     */
    CmUser getCmUserByUserId(Long userId);

}
