package cn.itbat.whh.usercenter.constant;

/**
 * 用户中心常量
 *
 * @author huahui.wu.
 * Created on 2018/3/16.
 */
public class Constants {

    //token key
    public static final String TOKEN = "token_key";
    public static final String CHAR_SET = "utf-8";
}
