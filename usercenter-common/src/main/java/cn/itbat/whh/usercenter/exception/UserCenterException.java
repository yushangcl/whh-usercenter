package cn.itbat.whh.usercenter.exception;

import cn.itbat.whh.usercenter.constant.UserCenterResultConstant;
import cn.itbat.whh.usercenter.enums.ErrorCode;

import java.text.MessageFormat;

/**
 * 用户中心异常
 *
 * @author huahui.wu.
 * Created on 2018/3/16.
 */
public class UserCenterException extends ServiceException {
    private Object[] params;

    public UserCenterException() {
        super();
    }

    public UserCenterException(UserCenterResultConstant result) {
        this(result.getCode(), result.getMessage());
    }

    public UserCenterException(UserCenterResultConstant result, Throwable cause) {
        this(result.getCode(), result.getMessage(), cause);
    }

    public UserCenterException(UserCenterResultConstant result, Object... params) {
        this(result.getCode(), MessageFormat.format(result.getMessage(), params));
        this.params = params;
    }

    public UserCenterException(UserCenterResultConstant result, Throwable cause, Object... params) {
        this(result.getCode(), MessageFormat.format(result.getMessage(), params), cause);
        this.params = params;
    }

    public UserCenterException(ErrorCode errorCode) {
        super(errorCode.getErrorCode(), errorCode.getErrorText());
    }

    public UserCenterException(ErrorCode errorCode, Object... params) {
        super(errorCode.getErrorCode(), errorCode.getErrorText(), params);
        this.params = params;
    }

    public UserCenterException(ErrorCode errorCode, Throwable cause) {
        super(errorCode.getErrorCode(), errorCode.getErrorText(), cause);
    }

    public UserCenterException(ErrorCode errorCode, Throwable cause, Object... params) {
        super(errorCode.getErrorCode(), errorCode.getErrorText(), cause, params);
        this.params = params;
    }

    public UserCenterException(int code, String message) {
        super(code, message);
    }

    public UserCenterException(int code, String message, Throwable cause) {
        super(code, message, cause);
    }

//    @Override
//    public String getMessage() {
//        MessageSource ms = (MessageSource)SpringContextUtil.getBean("messageSource");
//        String lan = RpcContext.getContext().getAttachment("lan");
//        Locale locale = null;
//        if (lan != null) {
//            locale = LocaleUtils.toLocale(lan);
//        } else {
//            locale = Locale.getDefault();
//        }
//        String message = ms.getMessage(String.valueOf(getCode()), params, super.getMessage(), locale);
//        return message;
//    }
}
