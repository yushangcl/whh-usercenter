package cn.itbat.whh.usercenter.constant;

/**
 * 用户中心返回值定义
 *
 * @author huahui.wu.
 * Created on 2018/3/16.
 */
public enum UserCenterResultConstant {

    FAILED(1, "failed"),
    SUCCESS(0, "success"),;

    public int code;

    public String message;

    UserCenterResultConstant(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
