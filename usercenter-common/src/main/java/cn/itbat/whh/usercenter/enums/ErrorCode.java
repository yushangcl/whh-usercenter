package cn.itbat.whh.usercenter.enums;

/**
 * 错误码
 *
 * @author huahui.wu.
 * Created on 2018/3/16.
 */
public enum ErrorCode {
    RequiredArgumentsMissing(105, "Missing required arguments"),
    InvalidArguments(110, "Invalid arguments"),
    MethodNotExist(140, "No Such Method"),
    SignError(302, "Check sign error"),
    ParseException(304, "Parse Data Exception"),
    AccessUnauthorized(330, "Access Unauthorized"),
    ApiCallError(540, "Api Call Error"),
    HostException(700, "Host Process Exception"),
    DATA_EXISTS(604, "{0}已存在，不允许重复"),
    DATA_NOT_EXISTS(605, "{0}不存在"),
    DATA_REQUIRED(606, "{0}不能为空"),
    INVALID_ARGS(609, "{0}为不合法的参数，建议值：{1}"),
    DATA_NOT_NUMBER(611, "传入的参数必须为数字：{0}"),
    DATA_MAX_LENGTH(612, "{0}长度不能大于{1}个字符"),
    DATA_MIN_LENGTH(613, "{0}长度不能小于{1}个字符"),
    DATA_RANGE(614, "{0}必须是 {1} 到 {2}之间的数字");

    private int code;
    private String text;

    private ErrorCode(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public int getErrorCode() {
        return this.code;
    }

    public String getErrorText() {
        return this.text;
    }

    @Override
    public String toString() {
        return this.getErrorCode() + ":" + this.getErrorText();
    }

    public static ErrorCode valueOf(int code) {
        ErrorCode[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            ErrorCode err = var1[var3];
            if (err.getErrorCode() == code) {
                return err;
            }
        }

        return null;
    }
}
