package cn.itbat.whh.usercenter.manager.mock;

import com.whh.common.mybatis.base.BaseManagerMock;
import cn.itbat.whh.usercenter.manager.BaDefinedCodeManager;
import cn.itbat.whh.usercenter.model.BaDefinedCode;
import cn.itbat.whh.usercenter.dao.model.BaDefinedCodeDOExample;

/**
* BaDefinedCodeManager
*  on 2018/4/13.
*/
public class BaDefinedCodeManagerMock extends BaseManagerMock<BaDefinedCode, BaDefinedCodeDOExample> implements BaDefinedCodeManager {

}
