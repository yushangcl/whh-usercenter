package cn.itbat.whh.usercenter.service.impl;

import cn.itbat.whh.usercenter.manager.CmUserManager;
import cn.itbat.whh.usercenter.model.CmUser;
import cn.itbat.whh.usercenter.service.CmUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author huahui.wu.
 *         Created on 2018/3/15.
 */
@Service
public class CmUserServiceImpl implements CmUserService {

    @Resource
    private CmUserManager cmUserManager;

    @Override
    public CmUser getCmUserByUserId(Long userId) {
        System.out.println("测试");
        return cmUserManager.getCmUserByUserId(userId);
    }
}
