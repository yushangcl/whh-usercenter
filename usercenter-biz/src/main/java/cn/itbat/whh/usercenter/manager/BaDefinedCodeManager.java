package cn.itbat.whh.usercenter.manager;

import com.whh.common.mybatis.base.BaseManager;
import cn.itbat.whh.usercenter.model.BaDefinedCode;
import cn.itbat.whh.usercenter.dao.model.BaDefinedCodeDOExample;

/**
* BaDefinedCodeManager
*  on 2018/4/13.
*/
public interface BaDefinedCodeManager extends BaseManager<BaDefinedCode, BaDefinedCodeDOExample> {

}