package cn.itbat.whh.usercenter;

import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * 启动工具类
 *
 * @author huahui.wu.
 * Created on 2018/3/15.
 */
public class BizApplication {

    public static void main(String[] args) throws InterruptedException {
        Logger logger = LoggerFactory.getLogger(BizApplication.class);

        logger.info(">>>>> usercenter 正在启动 <<<<<");
        try {
            ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring-config.xml");
            logger.info(">>>>> usercenter 启动完成 <<<<<");
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    logger.error(">>>>> xdw-usercenter-biz shutdown <<<<<");
                    applicationContext.close();
                }
            }));
            Thread.sleep(Long.MAX_VALUE);
        } catch (Exception e) {
            logger.error(">>>>> xdw-usercenter-biz 启动失败!! <<<<<", e);
        }

    }

    private static void setSysProp() {
        System.setProperty("java.security.egd", "file:/dev/../dev/urandom");
        if (System.getProperty("workdir") == null) {
            String runningJarPath = getRootFolder();
            int lastIndexOf = runningJarPath.lastIndexOf("/", runningJarPath.length() - 2);
            // jar包上级目录
            String workdir = runningJarPath.substring(0, lastIndexOf);
            System.setProperty("workdir", workdir);
        } else {
            String workdir = System.getProperty("workdir");
            if (workdir.endsWith("/")) {
                workdir = workdir.substring(0, workdir.length() - 1);
                System.setProperty("workdir", workdir);
            }
        }
        System.out.println("workdir:" + System.getProperty("workdir"));
    }

    private static String getRootFolder() {
        try {
            String runningJarPath = BizApplication.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath().replaceAll("\\\\", "/");
            int last = runningJarPath.lastIndexOf("/target/");
            if (last > 0) {
                runningJarPath = runningJarPath.substring(0, last);
                return runningJarPath;
            }
            if (!runningJarPath.endsWith("/")) {
                int lastIndexOf = runningJarPath.lastIndexOf("/");
                runningJarPath = runningJarPath.substring(0, lastIndexOf + 1);
            }
            return runningJarPath;
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static String getDisconfEnv() {
        // 从启动参数.
        String env = System.getProperty("disconf.env");
        if (env == null) {
            // 从配置文件
            Properties props = new Properties();
            InputStream in = null;
            try {
                in = BizApplication.class.getResourceAsStream("/disconf.properties");
                props.load(in);
                env = props.getProperty("env");
            } catch (Exception e) {
            } finally {
                if (null != in) {
                    try {
                        in.close();
                    } catch (IOException e) {
                    }
                }
            }
        }

        return env;
    }

    static {
        try {
            String env = getDisconfEnv();
            ConfigurationSource source;

            String relativePath = "log4j2.xml";
            if (env != null) {
                relativePath = "log4j2-" + env + ".xml";
            }
            InputStream inCfg = BizApplication.class.getClassLoader().getResourceAsStream(relativePath);
            if (inCfg != null) {
                source = new ConfigurationSource(inCfg);
                Configurator.initialize(null, source);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
