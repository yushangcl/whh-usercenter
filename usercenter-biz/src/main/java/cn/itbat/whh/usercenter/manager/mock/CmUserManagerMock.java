package cn.itbat.whh.usercenter.manager.mock;

import com.whh.common.mybatis.base.BaseManagerMock;
import cn.itbat.whh.usercenter.manager.CmUserManager;
import cn.itbat.whh.usercenter.model.CmUser;
import cn.itbat.whh.usercenter.dao.model.CmUserDOExample;

/**
* CmUserManager
*  on 2018/3/15.
*/
public class CmUserManagerMock extends BaseManagerMock<CmUser, CmUserDOExample> implements CmUserManager {

    @Override
    public CmUser getCmUserByUserId(Long userId) {
        return null;
    }
}
