package cn.itbat.whh.usercenter.manager;

import com.whh.common.mybatis.base.BaseManager;
import cn.itbat.whh.usercenter.model.CmUser;
import cn.itbat.whh.usercenter.dao.model.CmUserDOExample;

/**
* CmUserManager
*  on 2018/3/15.
*/
public interface CmUserManager extends BaseManager<CmUser, CmUserDOExample> {
    /**
     * 根据用户ID查询用户信息
     * @param userId 用户ID
     * @return CmUser
     */
    CmUser getCmUserByUserId(Long userId);
}