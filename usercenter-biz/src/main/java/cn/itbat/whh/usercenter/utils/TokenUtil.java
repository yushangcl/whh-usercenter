package cn.itbat.whh.usercenter.utils;


import cn.itbat.whh.usercenter.constant.Constants;
import com.whh.common.utils.util.CodecUtils;
import com.whh.common.utils.util.DateUtil;

import java.io.IOException;
import java.util.Date;

/**
 * token方法
 *
 * @author log.r
 * @date 2018-03-28 下午8:05
 **/
public class TokenUtil {
    /**
     * 生成 token
     *
     * @param userId 用户ID
     * @return token
     */
    public static String createToken(Long userId) throws IOException {
        String time = DateUtil.toDateString(new Date());
        String date = Constants.TOKEN + userId + time;
        return CodecUtils.getMD5(date, Constants.CHAR_SET, true);
    }
}
