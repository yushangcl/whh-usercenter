package cn.itbat.whh.usercenter.utils;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.utils.ReferenceConfigCache;
import com.alibaba.dubbo.rpc.service.GenericService;

import java.lang.reflect.UndeclaredThrowableException;

/**
 * @author huahui.wu.
 * Created on 2018/3/19.
 */

public class DubboUtil {

    private static Object invoke(String inter, String method, String[] invokeParamTypes, Object[] invokeParams) {
        // 普通编码配置方式
        ApplicationConfig application = new ApplicationConfig();
        application.setName("whh-service");

        // 连接注册中心配置
        RegistryConfig registry = new RegistryConfig();
        registry.setAddress("zookeeper://dev.itbat.cn");
        registry.setPort(2181);
        ReferenceConfig<GenericService> reference = new ReferenceConfig<GenericService>();
        reference.setApplication(application);
        reference.setRegistry(registry);
        reference.setInterface(inter);
        reference.setVersion("1.0.0.dev");
        reference.setGroup("impl");
        reference.setProtocol("dubbo");
        reference.setGeneric(true); // 声明为泛化接口

        ReferenceConfigCache cache = ReferenceConfigCache.getCache();
        GenericService genericService = cache.get(reference);
        Object result = genericService.$invoke(method, invokeParamTypes, invokeParams);

        System.out.println(result);
        return result;

    }

    public static void main(String[] args) {
        // 基本类型以及Date,List,Map等不需要转换，直接调用
        try {
            String[] invokeParamTypes = new String[1];
            Object[] invokeParams = new Object[1];
            invokeParamTypes[0] = "java.lang.String";
            invokeParams[0] = "张三";
            String inter = "com.whh.spring.boot.service.CmUserService";
            String method = "get";
            DubboUtil.invoke(inter, method, invokeParamTypes, invokeParams);

        } catch (UndeclaredThrowableException excep) {
            Throwable t = excep.getCause();
            if (t != null) {
                System.out.println(t.getMessage());
            }
        }


    }

}