package cn.itbat.whh.usercenter.service.mock;

import cn.itbat.whh.usercenter.model.CmUser;
import cn.itbat.whh.usercenter.service.CmUserService;

/**
 * Mock 类
 * @author huahui.wu.
 *         Created on 2018/3/15.
 */
public class CmUserServiceMock implements CmUserService {
    @Override
    public CmUser getCmUserByUserId(Long userId) {
        return null;
    }
}
