package cn.itbat.whh.usercenter.utils;

import cn.itbat.whh.usercenter.BaseTest;
import cn.itbat.whh.usercenter.model.CmUser;
import cn.itbat.whh.usercenter.service.CmUserService;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.data.redis.core.HashOperations;

import javax.annotation.Resource;

/**
 * @author huahui.wu.
 * Created on 2018/3/16.
 */
public class RedisTest extends BaseTest {

    private static final String CM_USER_REDIS_KEY = "CM_USER_REDIS";
    private static final String CM_USER_REDIS_KEY_STR = "CM_USER_REDIS_STR";
    private static final String CM_USER_REDIS_KEY_JSON = "CM_USER_REDIS_JSON";

    @Resource(name = "redisTemplate")
    private HashOperations<String, Long, CmUser> hashOperations;

    @Resource(name = "stringRedisTemplate")
    private HashOperations<String, String, CmUser> hashOperationsStr;

    @Resource(name = "jsonRedisTemplate")
    private HashOperations<String, String, CmUser> hashOperationsJson;
    @Resource
    private CmUserService cmUserService;

    @Test
    public void getCmUserFromRedis() throws Exception {
        CmUser cmUser = cmUserService.getCmUserByUserId(1L);
        hashOperations.put(CM_USER_REDIS_KEY, cmUser.getId(), cmUser);
        cmUser = hashOperations.get(CM_USER_REDIS_KEY, cmUser.getId());
        System.out.println(cmUser) ;
    }

    @Test
    public void getCmUserFromRedisString() throws Exception {
        CmUser cmUser = cmUserService.getCmUserByUserId(1L);
        hashOperationsStr.put(CM_USER_REDIS_KEY_STR, cmUser.getId().toString(), cmUser);
        cmUser = hashOperationsStr.get(CM_USER_REDIS_KEY_STR, cmUser.getId().toString());
        System.out.println(cmUser);
    }

    @Test
    public void getCmUserFromRedisJson() throws Exception {
        CmUser cmUser = cmUserService.getCmUserByUserId(1L);
        hashOperationsJson.put(CM_USER_REDIS_KEY_JSON, cmUser.getId().toString(), cmUser);
        cmUser = hashOperationsJson.get(CM_USER_REDIS_KEY_JSON, cmUser.getId().toString());
        System.out.println(cmUser);
    }
}
