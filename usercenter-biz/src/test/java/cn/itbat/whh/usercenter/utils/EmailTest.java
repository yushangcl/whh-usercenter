package cn.itbat.whh.usercenter.utils;

import cn.itbat.whh.usercenter.BaseTest;
import cn.itbat.whh.usercenter.utils.email.EmailUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author huahui.wu.
 * Created on 2018/4/11.
 */
public class EmailTest extends BaseTest {

    @Test
    public void send() throws Exception {
        String userName = "admin@edu-ing.ml"; // 发件人邮箱
        String password = "WHH88913233123"; // 发件人密码
        String smtpHost = "smtp.yandex.com"; // 邮件服务器

        String to = "rece@edu-ing.ml"; // 收件人，多个收件人以半角逗号分隔
        String cc = "rece1faf@edu-ing.ml"; // 抄送，多个抄送以半角逗号分隔
        String subject = "这是邮件的主题"; // 主题
        String body = "<h2 style='color:red'>这是邮件的正文</h2>"; // 正文，可以用html格式的哟
        List<String> attachments = Arrays.asList("D:\\test.txt"); // 附件的路径，多个附件也不怕
        EmailUtil email = EmailUtil.entity(smtpHost, userName, password, to, cc, subject, body, attachments);

        email.send(); // 发送！

    }
}
