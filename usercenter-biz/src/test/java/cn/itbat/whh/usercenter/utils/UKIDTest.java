package cn.itbat.whh.usercenter.utils;

import cn.itbat.whh.usercenter.BaseTest;
import com.whh.common.ukid.DOCN;
import com.whh.common.ukid.UKID;
import com.whh.common.ukid.UKIDOR;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @author huahui.wu. (;￢＿￢)
 * Created on 2018/4/19.
 */
public class UKIDTest extends BaseTest {

    @Test
    public void test() throws Exception {
        Set<Long> longs = new HashSet<>();

        for (int i = 0; i < 100; i++) {
            Long ukid = UKID.getUKID();
            longs.add(ukid);
            System.out.println(ukid);
        }
        System.out.println(longs.size());
    }

    @Test
    public void getBuID() throws Exception {
        Long buId = DOCN.getBuId();
        System.out.println(buId);
    }


    @Test
    public void getUserID() throws Exception {
        Long buId = DOCN.getEmpId();
        System.out.println(buId);
    }
}
