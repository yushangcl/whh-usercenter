package cn.itbat.whh.usercenter.service.impl;

import cn.itbat.whh.usercenter.BaseTest;
import cn.itbat.whh.usercenter.model.CmUser;
import cn.itbat.whh.usercenter.service.CmUserService;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * @author huahui.wu.
 *         Created on 2018/3/16.
 */
public class CmUserServiceImplTest extends BaseTest {

    @Resource
    private CmUserService cmUserService;

    @Test
    public void getCmUserByUserId() throws Exception {
        CmUser cmUser = cmUserService.getCmUserByUserId(1L);
        System.out.println(cmUser);
    }
}