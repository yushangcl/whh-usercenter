package cn.itbat.whh.usercenter.utils;

import cn.itbat.whh.usercenter.BaseTest;
import cn.itbat.whh.usercenter.model.CmUser;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.annotation.Resource;

/**
 * @author huahui.wu.
 * Created on 2018/4/10.
 */
public class MongodbTest extends BaseTest{
    @Resource
    @Qualifier("mongoTemplate")
    private MongoTemplate mongoTemplate;

    @Test
    public void test() {
        CmUser cmUser = new CmUser();
        cmUser.setId(2L);
        cmUser.setName("张三");
        mongoTemplate.insert(cmUser);
    }
}
